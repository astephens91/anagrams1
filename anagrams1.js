

const button = document.getElementById("findButton");
button.onclick = function () {
    let typedText = document.getElementById("input").value;
    getAnagramsOf(typedText);
    
}

    // my code goes here


    

    function getAnagramsOf(string){
        function alphabetize(a) {
            return a.toLowerCase().split("").sort().join("").trim();
        }
        
        let sorted = [];
        words.forEach((word) => sorted.push(alphabetize(word)));

        let indexes = [];
        sorted.forEach((word, i) =>{ if (alphabetize(string) == word){
            indexes.push(i)
        }})
                
        let results = [];
        indexes.forEach((word) => results.push(words[word]));
        let resultsSpaced = results.join(', ');
    
        let div = document.createElement("div");
        let node = document.createTextNode(resultsSpaced);
        div.appendChild(node);
        let element = document.getElementById("result");
        element.appendChild(div); 
        
        return results
    }